const { src, dest, parallel, series, watch } = require("gulp");
const browserSync = require("browser-sync").create();
const del = require("del");
const gulpif = require("gulp-if");
const cached = require("gulp-cached");
const argv = require("yargs").argv;
const pug = require("gulp-pug");
const plumber = require("gulp-plumber");
const validator = require("gulp-w3c-html-validator");
// const babel        = require("gulp-babel");
const concat = require("gulp-concat");
const uglify = require("gulp-uglify");
const scss = require("gulp-sass");
const autoprefixer = require("gulp-autoprefixer");
const sourcemaps = require("gulp-sourcemaps");
const cleanCSS = require("gulp-clean-css");
const imagemin = require("gulp-imagemin");

function browsersync() {
  browserSync.init({
    server: { baseDir: "public/" },
    notify: false,
    online: true,
  });
}

// function pug2html (){
//   return src('app/**/*.pug')
//   .pipe(cached('linting'))
//   .pipe(plumber())
//   .pipe(pug())
//   .pipe(plumber.stop())
//   .pipe(gulpif(argv.prod, validator()))
//   .pipe(dest("public"));
// }

function pug2html() {
  return src("app/**/*.html")
    .pipe(cached("linting"))
    .pipe(gulpif(argv.prod, validator()))
    .pipe(dest("public"))
    .pipe(browserSync.stream());
}

function styles() {
  return src("app/assets/styles/main.scss")
    .pipe(plumber())
    .pipe(gulpif(!argv.prod, sourcemaps.init()))
    .pipe(scss())
    .pipe(concat("main.min.css"))
    .pipe(
      gulpif(
        argv.prod,
        autoprefixer({ overrideBrowserslist: ["last 10 version"], grid: true })
      )
    )
    .pipe(
      gulpif(
        argv.prod,
        cleanCSS(
          {
            level: {
              2: {
                all: true,
                specialComments: 0,
              },
            },
            compatibility: "*",
            debug: true,
          },
          (details) => {
            console.log(`${details.name}: ${details.stats.originalSize}`);
            console.log(`${details.name}: ${details.stats.minifiedSize}`);
          }
        )
      )
    )
    .pipe(gulpif(!argv.prod, sourcemaps.write(".")))
    .pipe(dest("public/assets/css"))
    .pipe(browserSync.stream());
}

function fonts() {
  return src("app/assets/fonts/**/*.*").pipe(dest("public/assets/fonts"));
}

function scripts() {
  return (
    src(["app/assets/js/*.js"])
      .pipe(concat("app.min.js"))
      // .pipe(uglify())
      .pipe(dest("public/assets/js/"))
      .pipe(browserSync.stream())
  );
}

function libsjs() {
  return src([
    // "node_modules/svg4everybody/public/svg4everybody.min.js",
    "app/assets/js/libs/*.js",
  ])
    .pipe(concat("libs.min.js"))
    .pipe(uglify())
    .pipe(dest("public/assets/js/libs"))
    .pipe(browserSync.stream());
}

function imageMinify() {
  return (
    src([
      "app/assets/img/**/*.{gif,png,jpg,svg,webp}",
      // "!src/assets/img/sprite/**/*",
    ])
      // .pipe(buffer())
      .pipe(
        gulpif(
          argv.prod,
          imagemin([
            imagemin.gifsicle({ interlaced: true }),
            imagemin.mozjpeg({
              quality: 75,
              progressive: true,
            }),
            imagemin.optipng({ optimizationLevel: 5 }),
            imagemin.svgo({
              plugins: [{ removeViewBox: true }, { cleanupIDs: false }],
            }),
          ])
        )
      )
      .pipe(dest("public/assets/img/"))
  );
  // .pipe(browserSync.stream())
}

function startwatch() {
  watch(["app/**/*.html"], pug2html);
  watch(["app/assets/styles/**/*.scss"], styles);
  watch(["app/assets/fonts/**/*.*"], fonts);
  watch(["app/assets/js/*.js"], scripts);
  watch(["app/assets/js/libs/*.js"], libsjs);
  watch(["app/assets/img/**/*.{gif,png,jpg,svg,webp}"], imageMinify);
}

function clean(cb) {
  return del("public").then(() => {
    cb();
  });
}

exports.browsersync = browsersync;
exports.pug2html = pug2html;
exports.styles = styles;
exports.fonts = fonts;
exports.scripts = scripts;
exports.libsjs = libsjs;
exports.imageMinify = imageMinify;
exports.clean = clean;
exports.startwatch = startwatch;
exports.default = series(
  clean,
  parallel(pug2html, styles, fonts, scripts, libsjs, imageMinify),
  parallel(browsersync, startwatch)
);
