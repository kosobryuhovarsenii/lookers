$(document).ready(function () {
  // Бургер-меню
  const menuBtn = $(".header-left__button");
  const menu = menuBtn.siblings();

  menuBtn.on("click", function () {
    if (!menu.hasClass("active")) {
      menu.addClass("active");
    } else {
      menu.removeClass("active");
    }
  });

  // Страница каталога (сортировка)
  const sortBtn = $(".sort-item__header");
  const sortModal = sortBtn.siblings();

  sortBtn.on("click", function () {
    if ($(this).hasClass("active")) {
      sortBtn.removeClass("active");
      sortModal.fadeOut(200);
    } else {
      sortBtn.removeClass("active");
      sortModal.fadeOut(200);
      $(this).addClass("active");
      $(this).siblings().fadeIn(200);
    }
  });

  $('.title-sort__btn').on('click', function(e) {
    e.preventDefault();
    $('#sort').toggleClass('active')
  })

  // Корзина (офоромление товара)
  const getLabel = $(".get").find("label");
  const payLabel = $(".pay").find("label");

  getLabel.on("click", function () {
    deactivateCheckbox($(".get"), $(this));
  });
  payLabel.on("click", function () {
    deactivateCheckbox($(".pay"), $(this));
  });

  function deactivateCheckbox(block, item) {
    let inputs = block.find("input");
    let input = item.siblings();

    if (input.prop("checked")) return;
    inputs.prop("checked", false);
  }

  // новости
  $(".grid").masonry({
    itemSelector: ".grid-item",
    gutter: 30,
    horizontalOrder: true,
  });

  // вакансии
  $(".item-title").on("click", function () {
    $(".item-block").slideToggle();
    $(".item-header__block").slideToggle();
  });
});
