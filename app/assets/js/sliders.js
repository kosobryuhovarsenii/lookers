// Подключение сладйеров
$("document").ready(function () {
  // Сладйеры на главной
  $(".popular-slider").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: $(".popular-slider__prev"),
    nextArrow: $(".popular-slider__next"),
    responsive: [
      {
        breakpoint: 769,
        settings: {
          variableWidth: true,
          focusOnSelect: true,
          centerMode: true,
        },
      },
    ],
  });

  (function () {
    const slider = $(".main-popular__slider");
    const sliderMain = $(".main-popular__slider");

    if (!slider.length) {
      slider.slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        prevArrow: $(".main-popular__slider-prev"),
        nextArrow: $(".main-popular__slider-next"),
      });
    } else {
      sliderMain.slick({
        slidesToShow: 1,
        variableWidth: true,
        focusOnSelect: true,
        prevArrow: $(".main-popular__slider-prev"),
        nextArrow: $(".main-popular__slider-next"),
      });
    }
  })();

  $(".main-news__slider").slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    prevArrow: $(".main-news__prev"),
    nextArrow: $(".main-news__next"),
    responsive: [
      {
        breakpoint: 993,
        settings: {
          arrows: false,
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 769,
        settings: {
          variableWidth: true,
          focusOnSelect: true,
          centerMode: true,
        },
      },
    ],
  });

  // Коллекции
  $(".collect-menu__slider").slick({
    slidesToShow: 1,
    variableWidth: true,
    focusOnSelect: true,
    prevArrow: $(""),
    nextArrow: $(".collect-menu__slider-next"),
  });

  // Страница лояльности
  (function () {
    if (window.screen.width <= 768) return;

    $(".loyalty-slider").slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      prevArrow: $(".loyalty-slider__prev"),
      nextArrow: $(".loyalty-slider__next"),
    });
  })();

  // Меню страницы

  if (window.screen.width <= 992) {
    $(".left-menu").find("li:first").remove();

    $(".left-menu").slick({
      // loop: true,
      slidesToShow: 1,
      variableWidth: true,
      swipe: true,
    });
  }
});
